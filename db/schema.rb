# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170316063054) do

  create_table "b_evaluations", force: :cascade do |t|
    t.integer  "batting"
    t.integer  "run"
    t.integer  "shoulder"
    t.integer  "defense"
    t.string   "text"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "b_player_id"
  end

  create_table "b_players", force: :cascade do |t|
    t.integer  "team_id"
    t.string   "position"
    t.string   "name"
    t.date     "birthday"
    t.string   "graduate"
    t.integer  "height"
    t.integer  "weight"
    t.string   "blood_group"
    t.string   "handedness"
    t.string   "career"
    t.integer  "year"
    t.integer  "AVR"
    t.integer  "RBI"
    t.integer  "HR"
    t.integer  "SB"
    t.integer  "SH"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["team_id"], name: "index_b_players_on_team_id"
  end

  create_table "p_evaluations", force: :cascade do |t|
    t.integer  "speed"
    t.integer  "control"
    t.integer  "breaking"
    t.integer  "defense"
    t.string   "text"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "p_player_id"
  end

  create_table "p_players", force: :cascade do |t|
    t.integer  "team_id"
    t.string   "position"
    t.string   "name"
    t.date     "birthday"
    t.string   "graduate"
    t.integer  "height"
    t.integer  "weight"
    t.string   "blood_group"
    t.string   "handedness"
    t.string   "career"
    t.integer  "year"
    t.integer  "ERA"
    t.integer  "WIN"
    t.integer  "LOSE"
    t.integer  "SAVE"
    t.integer  "HOLD"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["team_id"], name: "index_p_players_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string   "name"
    t.string   "location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "homepage"
  end

end
