class CreateBPlayers < ActiveRecord::Migration[5.0]
  def change
    create_table :b_players do |t|
      t.references :team, foreign_key: true
      t.string :position
      t.string :name
      t.date :birthday
      t.string :graduate
      t.integer :height
      t.integer :weight
      t.string :blood_group
      t.string :handedness
      t.string :career
      t.integer :year
      t.integer :AVR
      t.integer :RBI
      t.integer :HR
      t.integer :SB
      t.integer :SH
      
      t.timestamps
    end
  end
end
