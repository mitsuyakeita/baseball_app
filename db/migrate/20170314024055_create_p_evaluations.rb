class CreatePEvaluations < ActiveRecord::Migration[5.0]
  def change
    create_table :p_evaluations do |t|
      t.integer :speed
      t.integer :control
      t.integer :breaking
      t.integer :defense
      t.string  :text
      t.timestamps
    end
  end
end
