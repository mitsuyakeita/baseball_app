class CreatePPlayers < ActiveRecord::Migration[5.0]
  def change
    create_table :p_players do |t|
      t.references :team, foreign_key: true
      t.string :position
      t.string :name
      t.date :birthday
      t.string :graduate
      t.integer :height
      t.integer :weight
      t.string :blood_group
      t.string :handedness
      t.string :career
      t.integer :year
      t.integer :ERA
      t.integer :WIN
      t.integer :LOSE
      t.integer :SAVE
      t.integer :HOLD
      

      t.timestamps
    end
  end
end
