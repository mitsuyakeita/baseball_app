class CreateBEvaluations < ActiveRecord::Migration[5.0]
  def change
    create_table :b_evaluations do |t|
      t.integer :batting
      t.integer :run
      t.integer :shoulder
      t.integer :defense
      t.string  :text
      t.timestamps
    end
  end
end
