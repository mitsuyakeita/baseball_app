class PPlayer < ApplicationRecord
  has_many :p_evaluations
    validate :add_error_sample
 
  def add_error_sample
    # nameが空のときにエラーメッセージを追加する
    if name.blank?
      errors[:base] << "選手名は必ず入力して下さい！"
    end
  end
end