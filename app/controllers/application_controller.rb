class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: "プロ野球選手の評価チェック！"
  end
end