class PPlayersController < ApplicationController
  def index
    @p_players = PPlayer.paginate(page: params[:page])
  end
  def show
    @p_player = PPlayer.find(params[:id])
    @p_evaluations = @p_player.p_evaluations.paginate(page: params[:page])
    @speed = @p_player.p_evaluations.average(:speed)
    @control = @p_player.p_evaluations.average(:control)
    @breaking = @p_player.p_evaluations.average(:breaking)
    @defense = @p_player.p_evaluations.average(:defense)
  end
  def new
    @p_player = PPlayer.new
  end
  def create
    @p_player = PPlayer.new(p_player_params)
    if @p_player.save
      flash[:info] = "選手を登録しました。"
      redirect_to p_players_path
    else
      render 'new'
    end
  end
  def edit
    @p_player = PPlayer.find(params[:id])
  end
  def update
    @p_player = PPlayer.find(params[:id])
    if @p_player.update_attributes(p_player_params)
       flash[:info] = "選手の編集完了！"
       redirect_to @p_player
    else
      render 'edit'
    end
  end
    private
    def p_player_params
      params.require(:p_player).permit(:name, :position, :team_id, :birthday, :graduate, :height, :weight, :blood_group, :handedness, :career, :year,)
    end
end
