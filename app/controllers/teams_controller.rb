class TeamsController < ApplicationController
  def show
    @team = Team.find(params[:id])
    @p_player = PPlayer.where("team_id = ?", @team.id)
    @b_player = BPlayer.where("team_id = ?", @team.id)
  end
end
