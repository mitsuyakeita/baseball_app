class BPlayersController < ApplicationController
  def index
    @b_players = BPlayer.paginate(page: params[:page])
  end
  def show
    @b_player = BPlayer.find(params[:id])
    @b_evaluations = @b_player.b_evaluations.paginate(page: params[:page])
    @batting = @b_player.b_evaluations.average(:batting)
    @run = @b_player.b_evaluations.average(:run)
    @shoulder = @b_player.b_evaluations.average(:shoulder)
    @defense = @b_player.b_evaluations.average(:defense)
  end
  def new
    @b_player = BPlayer.new
  end
  def create
    @b_player = BPlayer.new(b_player_params)
    if @b_player.save
      flash[:info] = "選手を登録しました。"
      redirect_to b_players_path
    else
      render 'new'
    end
  end
  def edit
    @b_player = BPlayer.find(params[:id])
  end
  def update
    @b_player = BPlayer.find(params[:id])
    if @b_player.update_attributes(b_player_params)
       flash[:info] = "選手の編集完了！"
       redirect_to @b_player
    else
      render 'edit'
    end
  end
    private
    def b_player_params
      params.require(:b_player).permit(:name, :position, :team_id, :birthday, :graduate, :height, :weight, :blood_group, :handedness, :career, :year,)
    end
end
