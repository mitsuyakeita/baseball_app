class PEvaluationsController < ApplicationController
  def index
    @p_evaluations = PEvaluation.paginate(page: params[:page])
  end
  def new
    @p_evaluation = PEvaluation.new
  end
  def create
    @p_evaluation = PEvaluation.new(p_evaluation_params)
    if @p_evaluation.save
      flash[:info] = "評価完了しました。"
      redirect_to p_player_path(params[:p_evaluation][:p_player_id])
    else
      render 'new'
    end
  end
  def edit
    @p_evaluation = PEvaluation.find(params[:id])
  end
  def update
    @p_evaluation = PEvaluation.find(params[:id])
    if @p_evaluation.update_attributes(p_evaluation_params)
      flash[:info] = "選手の評価編集完了！"
       redirect_to p_player_path(params[:p_evaluation][:p_player_id])
    else
      render 'edit'
    end
  end
    private
    def p_evaluation_params
      params.require(:p_evaluation).permit(:p_player_id, :speed, :control, :breaking, :defense, :text)
    end
end
