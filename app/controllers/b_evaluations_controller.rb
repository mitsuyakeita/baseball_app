class BEvaluationsController < ApplicationController
  def index
    @b_evaluations = BEvaluation.paginate(page: params[:page])
  end
  def new
    @b_evaluation = BEvaluation.new
  end
  def create
    @b_evaluation = BEvaluation.new(b_evaluation_params)
    if @b_evaluation.save
      flash[:info] = "評価完了しました。"
      redirect_to b_player_path(params[:b_evaluation][:b_player_id])
    else
      render 'new'
    end
  end
  def edit
    @b_evaluation = BEvaluation.find(params[:id])
  end
  def update
    @b_evaluation = BEvaluation.find(params[:id])
    if @b_evaluation.update_attributes(b_evaluation_params)
      flash[:info] = "選手の評価編集完了！"
       redirect_to b_player_path(params[:b_evaluation][:b_player_id])
    else
      render 'edit'
    end
  end
    private
    def b_evaluation_params
      params.require(:b_evaluation).permit(:b_player_id, :batting, :run, :shoulder, :defense, :text)
    end
end
