# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

custom_mapping_path = "mapping/japan.yml" # /path/to/mapping_data

JpPrefecture.setup do |config|
  config.mapping_data = YAML.load_file custom_mapping_path
end