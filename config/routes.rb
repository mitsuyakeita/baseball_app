Rails.application.routes.draw do

  root 'static_pages#home'
  resources :b_players
  resources :p_players
  resources :b_evaluations
  resources :p_evaluations
  resources :teams
end